package com.cointradeinfo.monitor.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class AdminSummary {
	private HashMap<String, Integer> tableSummary;
	private String usersPendingApprovals;
	private List<String> applicationErrors;
	private List<String> systemAlerts;
	private HashMap<String, HashMap<String, Integer>> requestMadePerExchangeObject;
	private HashMap<String, String> applicationHealth;
	private List<HashMap<String, String>> droppedMsgPerExchange;
	
	public AdminSummary() {
		// TODO Auto-generated constructor stub
	}

	public HashMap<String, Integer> getTableSummary() {
		return tableSummary;
	}

	public void setTableSummary(HashMap<String, Integer> tableSummary) {
		this.tableSummary = tableSummary;
	}

	public String getUsersPendingApprovals() {
		return usersPendingApprovals;
	}

	public void setUsersPendingApprovals(String usersPendingApprovals) {
		this.usersPendingApprovals = usersPendingApprovals;
	}

	public List<String> getApplicationErrors() {
		return applicationErrors;
	}

	public void setApplicationErrors(List<String> applicationErrors) {
		this.applicationErrors = applicationErrors;
	}

	public List<String> getSystemAlerts() {
		return systemAlerts;
	}

	public void setSystemAlerts(List<String> systemAlerts) {
		this.systemAlerts = systemAlerts;
	}

	public HashMap<String, String> getApplicationHealth() {
		return applicationHealth;
	}

	public void setApplicationHealth(HashMap<String, String> applicationHealth) {
		this.applicationHealth = applicationHealth;
	}

	public HashMap<String, HashMap<String, Integer>> getRequestMadePerExchangeObject() {
		return requestMadePerExchangeObject;
	}

	public void setRequestMadePerExchangeObject(
			HashMap<String, HashMap<String, Integer>> requestMadePerExchange) {
		this.requestMadePerExchangeObject = requestMadePerExchange;
	}
	
	public List<HashMap<String, String>> getDroppedMsgPerExchange() {
		return droppedMsgPerExchange;
	}

	public void setDroppedMsgPerExchange(
			List<HashMap<String, String>> droppedMsgPerExchange) {
		this.droppedMsgPerExchange = droppedMsgPerExchange;
	}

	public String toJson(){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("tableSummary", toJsonObject(getTableSummary()));
		jsonObj.put("applicationErrors",getApplicationErrors());
		jsonObj.put("systemAlerts", getSystemAlerts());
		jsonObj.put("applicationHealth", toJsonObject1(getApplicationHealth()));
		jsonObj.put("request_made_per_exchange", toJsonArray(getRequestMadePerExchangeObject()));
		jsonObj.put("dropped_msg_per_exchange", getDroppedMsgPerExchange());
		return jsonObj.toJSONString();
	}
	
	public Object toJsonObject1(HashMap<String, String> map) {
		JSONObject jsonObject = new JSONObject();
		for (Entry<String, String> entry : map.entrySet()) {
			 jsonObject.put(entry.getKey(),entry.getValue());
	    }
		return jsonObject;
	}

	public JSONArray toJsonArray(HashMap<String, HashMap<String, Integer>> map){
		JSONArray jsonArray = new JSONArray();
		 for (Entry<String, HashMap<String, Integer>> entry : map.entrySet()) {
			 JSONObject jsonObject = new JSONObject();
			 jsonObject.put("name", entry.getKey());
			 jsonObject.put("data", toJsonObject(entry.getValue()));
			 jsonArray.add(jsonObject);
	    }
		return jsonArray;
	}
	public JSONObject toJsonObject(HashMap<String, Integer> map){
		JSONObject jsonObject = new JSONObject();
		 for (Entry<String, Integer> entry : map.entrySet()) {
			 jsonObject.put(entry.getKey(),entry.getValue());
	    }
		return jsonObject;
	}
	
}
