package com.cointradeinfo.monitor.controller;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cointradeinfo.monitor.entity.GuestSummary;
import com.cointradeinfo.monitor.utilities.ObjectBuilder;

@RestController
public class GuestController {
	
	@Autowired
	ObjectBuilder objectBuilder; 
	
	@GetMapping("/summary")
	public String getSummary(){
		GuestSummary guestSummary = objectBuilder.guestSummaryBuild();
		return guestSummary.toJson();
	}
	
	@GetMapping("/orders/time/{time}")
	public JSONArray getordersandTradesByTime(@PathVariable("time") String time){
		HashMap<String, HashMap<String, Integer>> orders = objectBuilder.getOrdersByTime(time);
		return new GuestSummary().toJsonArray(orders);
	}

}
