package com.cointradeinfo.monitor.controller;

import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cointradeinfo.monitor.entity.AdminSummary;
import com.cointradeinfo.monitor.entity.GuestSummary;
import com.cointradeinfo.monitor.utilities.ObjectBuilder;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	ObjectBuilder objectBuilder; 
	
	@GetMapping("/health-check")
	public String healthCheck(){
		return "OK";
	}
	
	@GetMapping("/summary")
	public String getSummary(){
		return objectBuilder.adminSummaryBuild().toJson();
	}
	@GetMapping("/requests/time/{time}")
	public JSONArray getRequestPerExchnageByTime(@PathVariable("time") String time){
		HashMap<String, HashMap<String, Integer>> requests = objectBuilder.getRequestPerExchnageByTime(time);
		return new GuestSummary().toJsonArray(requests);
	}
	
	@GetMapping("/dropedmsg/time/{time}")
	public List<HashMap<String, String>> getDropedMsgPerExchnageByTime(@PathVariable("time") String time){
		return objectBuilder.getDropedMsgPerExchangeByTime(time);
	}
}
