package com.cointradeinfo.monitor.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import com.cointradeinfo.monitor.entity.AdminSummary;
import com.cointradeinfo.monitor.entity.GuestSummary;

@Component
public class ObjectBuilder {
	private static int flag = 1;
	private static final Logger logger = Logger.getLogger(ObjectBuilder.class);

	public GuestSummary guestSummaryBuild() {
		BufferedReader reader = null;
		try {
			if (flag % 2 == 0) {
				reader = new BufferedReader(
						new FileReader(
								"H://Project WorkSpace/monitor/monitoring-service/src/main/resources/json/guest/guest_01.json"));
			} else {
				reader = new BufferedReader(
						new FileReader(
								"H://Project WorkSpace/monitor/monitoring-service/src/main/resources/json/guest/guest.json"));
			}
			flag++;
			return parseGuestJson(reader);
			// return get(reader);

		} catch (FileNotFoundException e1) {
			logger.error("File not found: ");
			return null;
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private JSONArray get(BufferedReader reader) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(reader);
			JSONArray ordersTrdaesPerExchangJsonArray = (JSONArray) ((JSONObject) obj)
					.get("orders-trades-per-exchange");
			return ordersTrdaesPerExchangJsonArray;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private GuestSummary parseGuestJson(BufferedReader reader) {
		GuestSummary guestSummary = new GuestSummary();
		HashMap<String, Integer> countsMap = new HashMap<String, Integer>();
		HashMap<String, HashMap<String, Integer>> ordersTradesByExchange = new HashMap<String, HashMap<String, Integer>>();
		HashMap<String, HashMap<String, Integer>> ordersTradesByPair = new HashMap<String, HashMap<String, Integer>>();
		HashMap<String, HashMap<String, Integer>> ordersTradesByTime = new HashMap<String, HashMap<String, Integer>>();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) ((JSONObject) obj).get("JSON");
			JSONObject countsJson = (JSONObject) ((JSONObject) obj)
					.get("counts");
			countsMap.put("exchanges",
					Integer.parseInt(countsJson.get("exchanges").toString()));
			countsMap.put("currencies",
					Integer.parseInt(countsJson.get("currencies").toString()));
			countsMap.put("currency_pairs", Integer.parseInt(countsJson.get(
					"currency_pairs").toString()));
			countsMap.put("exchanges_supported_for_order_execution", Integer
					.parseInt(countsJson.get(
							"exchanges_supported_for_order_execution")
							.toString()));

			JSONArray ordersTrdaesPerExchangJsonArray = (JSONArray) ((JSONObject) obj)
					.get("orders-trades-per-exchange");
			for (int i = 0; i < ordersTrdaesPerExchangJsonArray.size(); i++) {
				HashMap<String, Integer> localMap = new HashMap<String, Integer>();
				String exchange = ((JSONObject) ordersTrdaesPerExchangJsonArray
						.get(i)).get("name").toString();
				JSONObject dataJson = (JSONObject) ((JSONObject) ordersTrdaesPerExchangJsonArray
						.get(i)).get("data");
				int orders = Integer
						.parseInt(dataJson.get("orders").toString());
				int trades = Integer
						.parseInt(dataJson.get("trades").toString());
				localMap.put("orders", orders);
				localMap.put("trades", trades);
				ordersTradesByExchange.put(exchange, localMap);
			}

			JSONArray ordersTradesbyTimeJson = (JSONArray) ((JSONObject) obj)
					.get("orders-trades-by-time");
			for (int i = 0; i < ordersTradesbyTimeJson.size(); i++) {
				HashMap<String, Integer> localMap = new HashMap<String, Integer>();
				String time = ((JSONObject) ordersTradesbyTimeJson.get(i)).get(
						"time").toString();
				JSONObject dataJson = (JSONObject) ((JSONObject) ordersTradesbyTimeJson
						.get(i)).get("data");
				int orders = Integer
						.parseInt(dataJson.get("orders").toString());
				int trades = Integer
						.parseInt(dataJson.get("trades").toString());
				localMap.put("orders", orders);
				localMap.put("trades", trades);
				ordersTradesByTime.put(time, localMap);
			}

			JSONArray ordersTradesPerPairsJson = (JSONArray) ((JSONObject) obj)
					.get("orders-trades-by-pair");
			for (int i = 0; i < ordersTradesPerPairsJson.size(); i++) {
				HashMap<String, Integer> localMap = new HashMap<String, Integer>();
				String pair = ((JSONObject) ordersTradesPerPairsJson.get(i))
						.get("name").toString();
				JSONObject dataJson = (JSONObject) ((JSONObject) ordersTradesPerPairsJson
						.get(i)).get("data");
				int orders = Integer
						.parseInt(dataJson.get("orders").toString());
				int trades = Integer
						.parseInt(dataJson.get("trades").toString());
				localMap.put("orders", orders);
				localMap.put("trades", trades);
				ordersTradesByPair.put(pair, localMap);
			}
			guestSummary.setTableSummary(countsMap);
			guestSummary.setOrdersTradesByExchange(ordersTradesByExchange);
			guestSummary.setOrdersTradesByTime(ordersTradesByTime);
			guestSummary.setOrdersTradesByCurrencyPair(ordersTradesByPair);
			reader.close();
			System.out.println("finish");
		} catch (Exception e) {
		}
		return guestSummary;
	}

	public AdminSummary adminSummaryBuild() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(
					new FileReader(
							"H://Project WorkSpace/monitor/monitoring-service/src/main/resources/json/admin/admin.json"));
			return parseAdminJson(reader);
		} catch (FileNotFoundException e1) {
			logger.error("File not found: ");
			return null;
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private AdminSummary parseAdminJson(BufferedReader reader) {
		AdminSummary adminSummary = new AdminSummary();
		HashMap<String, Integer> countsMap = new HashMap<String, Integer>();
		List<HashMap<String, String>> dropMsgPerExchangeList = new ArrayList<HashMap<String,String>>();
		HashMap<String, HashMap<String, HashMap<String, Integer>>> requestMadePerExchangeObject = new HashMap<String, HashMap<String, HashMap<String, Integer>>>();
		List<String> applicationErrors = new ArrayList<String>();
		List<String> systemAlerts = new ArrayList<String>();
		HashMap<String, String> applicationHealth = new HashMap<String, String>();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(reader);
			JSONObject countsJson = (JSONObject) ((JSONObject) obj)
					.get("counts");
			countsMap.put("exchanges",
					Integer.parseInt(countsJson.get("exchanges").toString()));
			countsMap.put("currencies",
					Integer.parseInt(countsJson.get("currencies").toString()));
			countsMap.put("currency_pairs", Integer.parseInt(countsJson.get(
					"currency_pairs").toString()));
			countsMap.put("exchanges_supported_for_order_execution", Integer
					.parseInt(countsJson.get(
							"exchanges_supported_for_order_execution")
							.toString()));

			JSONArray dropMsgPerExchangeArray = (JSONArray) ((JSONObject) obj)
					.get("dropped_msg_per_exchange");
			
			for (int i = 0; i < dropMsgPerExchangeArray.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				String exchange = ((JSONObject) dropMsgPerExchangeArray
						.get(i)).get("name").toString();
				String data = ((JSONObject) dropMsgPerExchangeArray
						.get(i)).get("data").toString();
				map.put("name", exchange);
				map.put("data", data);
				dropMsgPerExchangeList.add(map);
			}
			
			JSONArray requestMadePerExchangeArray = (JSONArray) ((JSONObject) obj)
					.get("request_made_per_exchange");
			HashMap<String, HashMap<String, Integer>> requestMadePerExchange = new HashMap<String, HashMap<String, Integer>>();
			System.out.println(requestMadePerExchangeArray);
			for (int i = 0; i < requestMadePerExchangeArray.size(); i++) {
				HashMap<String, Integer> localMap = new HashMap<String, Integer>();
				String exchange = ((JSONObject) requestMadePerExchangeArray
						.get(i)).get("name").toString();
				JSONObject dataJson = (JSONObject) ((JSONObject) requestMadePerExchangeArray
						.get(i)).get("data");
				int orders = Integer.parseInt(dataJson.get("success")
						.toString());
				int trades = Integer
						.parseInt(dataJson.get("failed").toString());
				localMap.put("success", orders);
				localMap.put("failed", trades);
				requestMadePerExchange.put(exchange, localMap);
			}
			requestMadePerExchangeObject.put("request_made_per_exchange", requestMadePerExchange);
			
			JSONArray applicationErrorsArray = (JSONArray) ((JSONObject) obj)
					.get("application_errors");
			for (int i = 0; i < applicationErrorsArray.size(); i++) {
				applicationErrors.add(((JSONObject) applicationErrorsArray
						.get(i)).get("error").toString());
			}

			JSONArray systemAlertArray = (JSONArray) ((JSONObject) obj)
					.get("system_alert");
			for (int i = 0; i < systemAlertArray.size(); i++) {
				systemAlerts.add(((JSONObject) systemAlertArray.get(i)).get(
						"error").toString());
			}

			JSONObject applicationHealthObj = (JSONObject) ((JSONObject) obj)
					.get("application_health");
			applicationHealth.put("cointradeinfo",
					applicationHealthObj.get("cointradeinfo").toString());
			applicationHealth.put("attribution",
					applicationHealthObj.get("attribution").toString());
			applicationHealth.put("marketdata_order_persistance",
					applicationHealthObj.get("marketdata_order_persistance")
							.toString());
			applicationHealth.put("execution",
					applicationHealthObj.get("execution").toString());
			applicationHealth.put("rabitmq", applicationHealthObj
					.get("rabitmq").toString());
			applicationHealth.put("mysql", applicationHealthObj.get("mysql")
					.toString());
			adminSummary.setTableSummary(countsMap);
			adminSummary.setSystemAlerts(systemAlerts);
			adminSummary.setApplicationErrors(applicationErrors);
			adminSummary.setApplicationHealth(applicationHealth);
			adminSummary.setRequestMadePerExchangeObject(requestMadePerExchange);
			adminSummary.setDroppedMsgPerExchange(dropMsgPerExchangeList);

		} catch (Exception e) {
			System.out.println(e);
		}
		return adminSummary;
	}

	public HashMap<String, HashMap<String, Integer>> getOrdersByTime(String requestedtime) {
		HashMap<String, HashMap<String, Integer>> ordersTradesByTime = new HashMap<String, HashMap<String, Integer>>();
		try {
			
			BufferedReader reader = new BufferedReader(
					new FileReader(
							"H://Project WorkSpace/monitor/monitoring-service/src/main/resources/json/guest/odersbyTime.json"));
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONArray ordersTradesbyTimeJson = (JSONArray) ((JSONObject) obj)
					.get(requestedtime);
			for (int i = 0; i < ordersTradesbyTimeJson.size(); i++) {
				HashMap<String, Integer> localMap = new HashMap<String, Integer>();
				String time = ((JSONObject) ordersTradesbyTimeJson.get(i)).get(
						"time").toString();
				JSONObject dataJson = (JSONObject) ((JSONObject) ordersTradesbyTimeJson
						.get(i)).get("data");
				int orders = Integer
						.parseInt(dataJson.get("orders").toString());
				int trades = Integer
						.parseInt(dataJson.get("trades").toString());
				localMap.put("orders", orders);
				localMap.put("trades", trades);
				ordersTradesByTime.put(time, localMap);
			}
		} catch (Exception e) {

		}
		return ordersTradesByTime;
	}
	
	public HashMap<String, HashMap<String, Integer>> getRequestPerExchnageByTime(String requestedtime) {
		HashMap<String, HashMap<String, Integer>> ordersTradesByTime = new HashMap<String, HashMap<String, Integer>>();
		try {
			
			BufferedReader reader = new BufferedReader(
					new FileReader(
							"H://Project WorkSpace/monitor/monitoring-service/src/main/resources/json/admin/requestPerExchangebyTime.json"));
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONArray ordersTradesbyTimeJson = (JSONArray) ((JSONObject) obj)
					.get(requestedtime);
			for (int i = 0; i < ordersTradesbyTimeJson.size(); i++) {
				HashMap<String, Integer> localMap = new HashMap<String, Integer>();
				String name = ((JSONObject) ordersTradesbyTimeJson.get(i)).get(
						"name").toString();
				JSONObject dataJson = (JSONObject) ((JSONObject) ordersTradesbyTimeJson
						.get(i)).get("data");
				int orders = Integer
						.parseInt(dataJson.get("success").toString());
				int trades = Integer
						.parseInt(dataJson.get("failed").toString());
				localMap.put("success", orders);
				localMap.put("failed", trades);
				ordersTradesByTime.put(name, localMap);
			}
		} catch (Exception e) {

		}
		return ordersTradesByTime;
	}
	
	public List<HashMap<String, String>> getDropedMsgPerExchangeByTime(String requestedTime){
		try{
		BufferedReader reader = new BufferedReader(
				new FileReader(
						"H://Project WorkSpace/monitor/monitoring-service/src/main/resources/json/admin/dropedMsgPreMsgByTime.json"));
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(reader);
		List<HashMap<String, String>> dropMsgPerExchangeList = new ArrayList<HashMap<String,String>>();
		JSONArray dropMsgPerExchangeArray = (JSONArray) ((JSONObject) obj)
				.get(requestedTime);
		
		for (int i = 0; i < dropMsgPerExchangeArray.size(); i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			String exchange = ((JSONObject) dropMsgPerExchangeArray
					.get(i)).get("name").toString();
			String data = ((JSONObject) dropMsgPerExchangeArray
					.get(i)).get("data").toString();
			map.put("name", exchange);
			map.put("data", data);
			dropMsgPerExchangeList.add(map);
		}
		return dropMsgPerExchangeList;
		}catch(Exception e){
			
		}
		return null;
	}
}
