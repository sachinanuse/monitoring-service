package com.cointradeinfo.monitor.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class GuestSummary {
	private HashMap<String, Integer> tableSummary;
	private HashMap<String, HashMap<String, Integer>> ordersTradesByExchange;
	private HashMap<String, HashMap<String, Integer>> ordersTradesByTime;
	private HashMap<String, HashMap<String, Integer>> ordersTradesByCurrencyPair;
	public GuestSummary() {
		// TODO Auto-generated constructor stub
	}
	public GuestSummary(HashMap<String, Integer> tableSummary,
			HashMap<String, HashMap<String, Integer>> ordersTradesByExchange,
			HashMap<String, HashMap<String, Integer>> ordersTradesByTime,
			HashMap<String, HashMap<String, Integer>> ordersTradesByCurrencyPair) {
		super();
		this.tableSummary = tableSummary;
		this.ordersTradesByExchange = ordersTradesByExchange;
		this.ordersTradesByTime = ordersTradesByTime;
		this.ordersTradesByCurrencyPair = ordersTradesByCurrencyPair;
	}
	public HashMap<String, Integer> getTableSummary() {
		return tableSummary;
	}
	public void setTableSummary(HashMap<String, Integer> tableSummary) {
		this.tableSummary = tableSummary;
	}
	public HashMap<String, HashMap<String, Integer>> getOrdersTradesByExchange() {
		return ordersTradesByExchange;
	}
	public void setOrdersTradesByExchange(
			HashMap<String, HashMap<String, Integer>> ordersTradesByExchange) {
		this.ordersTradesByExchange = ordersTradesByExchange;
	}
	public HashMap<String, HashMap<String, Integer>> getOrdersTradesByTime() {
		return ordersTradesByTime;
	}
	public void setOrdersTradesByTime(
			HashMap<String, HashMap<String, Integer>> ordersTradesByTime) {
		this.ordersTradesByTime = ordersTradesByTime;
	}
	public HashMap<String, HashMap<String, Integer>> getOrdersTradesByCurrencyPair() {
		return ordersTradesByCurrencyPair;
	}
	public void setOrdersTradesByCurrencyPair(
			HashMap<String, HashMap<String, Integer>> ordersTradesByCurrencyPair) {
		this.ordersTradesByCurrencyPair = ordersTradesByCurrencyPair;
	}
	
	public String toJson(){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("tableSummary", toJsonObject(getTableSummary()));
		jsonObj.put("ordersTradesByExchange", toJsonArray(getOrdersTradesByExchange()));
		jsonObj.put("ordersTradesByCurrencyPair", toJsonArray(getOrdersTradesByCurrencyPair()));
		jsonObj.put("ordersTradesByTime", toJsonArray(getOrdersTradesByTime()));
		return jsonObj.toJSONString();
	}
	public JSONArray toJsonArray(HashMap<String, HashMap<String, Integer>> map){
		JSONArray jsonArray = new JSONArray();
		 for (Entry<String, HashMap<String, Integer>> entry : map.entrySet()) {
			 JSONObject jsonObject = new JSONObject();
			 jsonObject.put("name", entry.getKey());
			 jsonObject.put("data", toJsonObject(entry.getValue()));
			 jsonArray.add(jsonObject);
	    }
		return jsonArray;
	}
	public JSONObject toJsonObject(HashMap<String, Integer> map){
		JSONObject jsonObject = new JSONObject();
		 for (Entry<String, Integer> entry : map.entrySet()) {
			 jsonObject.put(entry.getKey(),entry.getValue());
	    }
		return jsonObject;
	}

}
